import { describe, it, expect } from "vitest";
import { double } from "./doubler";

describe("doubler", () => {
  describe("double", () => {
    it("should dobule the value", () => {
      const result = double(1);
      expect(result).toEqual(2);
    });
  });
});
