import { useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";
import { double } from "./core/doubler";

function App() {
  const [count, setCount] = useState(0);

  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank" rel="noreferrer">
          <img
            src={`${import.meta.env["BASE_URL"]}/vite.svg`}
            className="logo"
            alt="Vite logo"
          />
        </a>
        <a href="https://reactjs.org" target="_blank" rel="noreferrer">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>double count is {double(count)}</p>
        <p>
          Edit <code>src/App.tsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>

      <h4>Project Name: {import.meta.env["VITE_PROJECT_NAME"]}</h4>
    </div>
  );
}

export default App;
